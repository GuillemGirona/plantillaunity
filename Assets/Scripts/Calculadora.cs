﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Calculadora {

    public static string textoAyuda = "HELP";

    private static void MuestraAyuda(string texto)
    {
        Debug.Log(textoAyuda + ":" + texto);
    }

       public static int Suma(int numero1, int numero2)
        {
        Calculadora.MuestraAyuda("Suma Entera");
        return numero1 + numero2;
        }

    public static int Resta(int numero1, int numero2)
    {
        Debug.Log("Resta Entera");
        return numero1 - numero2;
    }

    public static int Multiplicar(int numero1, int numero2)
    {
        Debug.Log("Multiplicacion Entera");
        return numero1 * numero2;
    }

    public static int Dividir(int numero1, int numero2)
    {
        Debug.Log("Suma Entera");
        return numero1 / numero2;
    }
    public static float Suma(float numero1, float numero2)
    {
        return numero1 + numero2;
    }

    public static float Resta(float numero1, float numero2)
    {
        return numero1 - numero2;
    }

    public static float Multiplicar(float numero1, float numero2)
    {
        return numero1 * numero2;
    }

    public static float Dividir(float numero1, float numero2)
    {
        return numero1 / numero2;
    }
}

