﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    public Vector2 speed;
    public float maxVelocidad;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(speed.x*Time.deltaTime, speed.y * Time.deltaTime, 0);
	}

    void OnTriggerEnter(Collider other)
    {
        speed.x *= 1.25f;
        speed.y *= 1.25f;
        if (speed.x > maxVelocidad)
        {
            speed.x = maxVelocidad;
        }
        if(speed.y > maxVelocidad)
        {
            speed.y = maxVelocidad;
        }
        if(speed.x < -maxVelocidad)
        {
            speed.x = -maxVelocidad;
        }
        if(speed.y < -maxVelocidad)
        {
            speed.y = -maxVelocidad;
        }

        switch(other.tag)
        {
            case "Player":
                speed.x = - speed.x;
                break;
            case "UpDown":
                speed.y = - speed.y;
                break;
            case "Left":
                speed.x = - speed.x;
                break;
            case "Right":
                speed.x = - speed.x;
                break;
        }
    }
}
