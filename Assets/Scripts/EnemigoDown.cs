﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDown : MonoBehaviour
{

    private Transform playerTransform;
    private Vector3 newPos;

    public Transform posBall;

    public float speed;
    private float moveHorizontal;

    void Start()
    {
        // Equivalente a playerTransform = GetComponent<Transform> ();
        playerTransform = transform;

        newPos = playerTransform.position;
    }

    void Update()
    {
        if (posBall.position.x > transform.position.x)
        {
            moveHorizontal = 1;
        }
        else
        {
            moveHorizontal = -1;
        }
        moveHorizontal *= Time.deltaTime;
        moveHorizontal *= speed;
        transform.Translate(moveHorizontal, 0, 0);

        if (transform.position.x < -17.0f)
        {
            transform.position = new Vector3(-9.0f, transform.position.y, transform.position.z);
        }
        else if (transform.position.x > 17.0f)
        {
            transform.position = new Vector3(9.0f, transform.position.y, transform.position.z);
        }
    }
}