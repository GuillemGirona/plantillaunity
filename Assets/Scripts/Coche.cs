﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coche : Vehiculo {
    public bool familiar = false;
    public void esFamiliar()
    {
        if(familiar)
        {
            Debug.Log("Es Familiar");
        }
        else
        {
            Debug.Log("No es Familiar");
        }
    }

    public override void Matricula()
    {
        Debug.Log("Matricula B0247BJ");
    }

    public override void MuestraDatosHeredados()
    {
        Debug.Log("MuestraDatosDerivados");
    }
}

