﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestVehiculo : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Moto moto = new Moto();
        moto.Caballito();
        moto.Matricula();

        Coche coche = new Coche();
        coche.MuestraDatos();
        coche.esFamiliar();
        coche.Matricula();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
