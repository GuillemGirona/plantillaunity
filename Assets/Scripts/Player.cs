﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float vely;
    public float maxy;
    private float moveVertical;
    private Vector3 positionActual;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        moveVertical = Input.GetAxis("Vertical");
        transform.Translate(0, vely * moveVertical * Time.deltaTime, 0);
        positionActual = transform.position;
        if(positionActual.y > maxy)
        {
            positionActual.y = maxy;
        }
        else if(positionActual.y < -maxy)
        {
            positionActual.y = -maxy;
        }
        transform.position = positionActual;
    }
}
