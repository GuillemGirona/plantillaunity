﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Vehiculo {

    public string nombre = "Vehiculo";
    public int numRuedas = 4;

    public void MuestraDatos()
    {
        Debug.Log("Nombre "+" = " +nombre);
        Debug.Log("NumRuedas: " + numRuedas);
    }

    public abstract void Matricula();
    public abstract void MuestraDatosHeredados();
}
