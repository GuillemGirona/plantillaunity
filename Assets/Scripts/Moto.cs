﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moto : Vehiculo {
    public new string nombre = "Moto";
    public new int numRuedas = 2;

    public void Caballito()
    {
        Debug.Log("Hago Caballito" +numRuedas);
    }

    public override void Matricula()
    {
        Debug.Log("Matricula 4898JCB");
    }

    public override void MuestraDatosHeredados()
    {
        Debug.Log("MuestraDatosDerivados");
    }
}
