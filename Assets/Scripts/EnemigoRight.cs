﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoRight : MonoBehaviour
{

    public float vely;
    public float maxy;
    private float moveVertical;
    public Transform posBall;
    public float speed;

    // Update is called once per frame
    void Update()
    {

        if (posBall.position.x > 0)
        {
            if (posBall.position.y > transform.position.y)
            {
                moveVertical = 1;
            }
            else if (posBall.position.y < transform.position.y)
            {
                moveVertical = -1;
            }
            else
            {
                moveVertical = 0;
            }
        }
        moveVertical *= Time.deltaTime;
        moveVertical *= speed;
        transform.Translate(0, moveVertical, 0);

    }
}
